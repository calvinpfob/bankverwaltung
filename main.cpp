#include "user.h"
#include "account.h"
#include"Bank_Giro.h"
#include"Building_Loan_Contract.h"
#include"MoneyMarketAccount.h"
#include <iostream>
void user_log_in(int user);
void control_center(void);
#define max_Number 10
#define max_bank_acc 200
using namespace std;
User *user[max_Number];
Bank_Giro *bank_Giro[max_bank_acc];
Building_Loan_Contract *building_Loan_Contract[max_bank_acc];
MoneyMarketAccount *money_Market_Account[max_bank_acc];

void see_all_bank_accounts(int user)
{
	for (int i = 0; i < max_bank_acc; i++)
	{
		if (bank_Giro[i]->getOwner() == user)
		{
			cout << "\nBLZ: " << bank_Giro[i]->getBLZ() << "\nAccount number: " << bank_Giro[i]->getAccountNumber() << "\nType of Account: " << bank_Giro[i]->getName() << "\nCredit: " << bank_Giro[i]->getCredit() << endl;
		}
		if (building_Loan_Contract[i]->getOwner()==user)
		{
			cout << "\nBLZ: " << building_Loan_Contract[i]->getBLZ() << "\nAccount number: " << building_Loan_Contract[i]->getAccountNumber() << "\nType of Account: " << building_Loan_Contract[i]->getName() << "\nCredit: " << building_Loan_Contract[i]->getCredit() << endl;
		
		}
		if (money_Market_Account[i]->getOwner() == user)
		{
			cout << "\nBLZ: " << money_Market_Account[i]->getBLZ() << "\nAccount number: " << money_Market_Account[i]->getAccountNumber() << "\nType of Account: " << money_Market_Account[i]->getName() << "\nCredit: " << money_Market_Account[i]->getCredit() << endl;
		}
	}
	user_log_in(user);
}

void create_new_bank_account(int user)
{
	int menu;
	static int counter_Giro_Bank_Account=0;
	static int counter_Building_Loan_Contract=0;
	static int counter_Money_Market_Account=0;
	cout << "Which bank account do you want to create?\n 1) Giro Bank Account \n 2) Building Loan Contract\n 3) Money Market Account ?\n";
	cin >> menu;
	if (menu == 1)
	{
		bank_Giro[counter_Giro_Bank_Account]->setOwner(user);
		counter_Giro_Bank_Account++;
	}
	else if (menu == 2)
	{
		building_Loan_Contract[counter_Building_Loan_Contract]->setOwner(user);
		counter_Building_Loan_Contract++;
	}
	else if (menu == 3)
	{
		money_Market_Account[counter_Money_Market_Account]->setOwner(user);
		counter_Money_Market_Account++;
	}
	user_log_in(user);
}

void user_log_in(int user) //possibilities for an user after he logged in
{
	int menu;
	cout << "Do you want to see all other accounts(1), create a new account(2) or log off(3)\n";
	cin >> menu;
	switch (menu)
	{
	case 1: see_all_bank_accounts(user); break;
	case 2: create_new_bank_account(user); break;
	case 3: cout << "You're logged off!"; control_center(); break;
	default: cout << "Something went wrong\n"; break;
	}
}

void makeUser(void)
{
	string username;
	string password;
	static int i = 0;
	cout << "Please type in your username...\n";
	cin >> username;
	cout << "Please type in your password...\n";
	cin >> password;
	user[i]->set_username(username);
	user[i]->set_password(password);
	i++;
}

void signIn(void)
{
	int stop=0;
	string username;
	string password;
	while (stop == 0)
	{

		cout << "Please type in your username...\n";
		cin >> username;
		cout << "Please type in your password...\n";
		cin >> password;
		for (int i = 0; i < max_Number; i++)
		{
			if ((user[i]->get_username() == username) && (user[i]->get_password() == password))
			{
				cout << "Welcome " << username << "\n";
				stop = 1;
				user_log_in(i);
				break;
			}
			else if (i == max_Number - 1)
			{
				cout << "Wrong username or password. Try it again. \n ";
			}
		}

	}
		
}

void control_center(void)
{
	static int menu;
	
	for (;;)
	{
		if (menu == 3) break;
		cout << "Do you want to register (1), to sign in (2) or to close the programm (3)?\n";
		cin >> menu;

		switch (menu)
		{
		case 1: makeUser(); break;
		case 2: 		
		if (user[0]->get_username() == "")//no user was created when first username is empty
		{
			cout << "Please create an user first.\n";
		}
		else
		{
			signIn(); break;
		}
		case 3: break;
		default: "There was a problem. Try it again"; break;
		}
		if (menu == 3)break;
	}
}

void main (void)
{
	for (int i = 0; i < max_Number; i++)
	{
		 user[i]= new User();
	}
	for (int i = 0; i < max_bank_acc; i++)
	{
		bank_Giro[i] = new Bank_Giro();
		building_Loan_Contract[i]=new Building_Loan_Contract();
		money_Market_Account[i]=new MoneyMarketAccount();
	}
	control_center();
}