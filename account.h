#pragma once
#include <iostream>
#include<string>
#include <ctime>
using namespace std;


class Account
{
public:
	Account();
	~Account();
	string getName();
	string getDescription();
	int getID();
	int getAccountNumber();
	int getBLZ();
	string getContactName();
	int getOwner();
	void setOwner(int owner);
	int getCredit();
	int getAccountOpeningDate();
	double getinterestRate();
protected:
	string name_;
	string description_;
	int ID_;
	mutable int accountNumber_;
	int BLZ_;
	string contactName_;
	int owner_;
	int credit_;
	int accountOpeningDate_;
	double interestRate_; //Zinssatz
	
	
};

