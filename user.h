#pragma once
#include <iostream>
#include<string>
using namespace std;

class User
{
private:
	string username_;
	string password_;
public:
	string User::get_username();
	string User::get_password();
	void User::set_username(string username);
	User(string username, string password);
	User();
	void User::set_password(string password);
	~User();
};

